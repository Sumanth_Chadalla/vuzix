package com.abi.sme.a.utils;

import android.os.Bundle;

import com.abi.sme.a.R;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBEventType;
import com.quickblox.messages.model.QBNotificationType;
import com.quickblox.messages.model.QBPushType;
import com.quickblox.sample.core.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tereha on 13.05.16.
 */
public class PushNotificationSender {

//    public static void sendPushMessage(ArrayList<Integer> recipients, Integer id, String senderName) {
//        String outMessage = String.format(String.valueOf(R.string.text_push_notification_message), senderName);
//
//        // Send Push: create QuickBlox Push Notification Event
//        QBEvent qbEvent = new QBEvent();
//        qbEvent.setNotificationType(QBNotificationType.PUSH);
//        qbEvent.setEnvironment(QBEnvironment.PRODUCTION);
//        qbEvent.setType(QBEventType.ONE_SHOT);
//        qbEvent.setId(id);
//
//        // Generic push - will be delivered to all platforms (Android, iOS, WP, Blackberry..)
//        qbEvent.setMessage(outMessage);
//
//        StringifyArrayList<Integer> userIds = new StringifyArrayList<>(recipients);
//        qbEvent.setUserIds(userIds);
//
//        QBPushNotifications.createEvent(qbEvent).performAsync(new QBEntityCallback<QBEvent>() {
//            @Override
//            public void onSuccess(QBEvent qbEvent, Bundle args) {
//                // sent
//                Toaster.shortToast("call notification success");
//            }
//
//            @Override
//            public void onError(QBResponseException errors) {
//                Toaster.shortToast("call notification fail");
//            }
//        });
//    }

    public static void sendPushMessage(ArrayList<Integer> recipients, String senderName, String userId,
                                       String groupCall, String videoCall, String sessionID, String email, int userQbId) {
        String outMessage = senderName + " is calling you";

        // Send Push: create QuickBlox Push Notification Event
        QBEvent qbEvent = new QBEvent();
        qbEvent.setNotificationType(QBNotificationType.PUSH);
        qbEvent.setEnvironment(QBEnvironment.PRODUCTION);
        // Generic push - will be delivered to all platforms (Android, iOS, WP, Blackberry..)
//        qbEvent.setMessage(outMessage);



        JSONObject json = new JSONObject();
        try {
            json.put("type", "call");
            json.put("fromName", senderName);
            json.put("userId", userId);
            json.put("groupCall", "no");
            json.put("videoCall", videoCall);
            json.put("sessionID", sessionID);
            json.put("Email", email);

        } catch (Exception e) {
            e.printStackTrace();
        }


        qbEvent.setMessage(String.valueOf(json));
        StringifyArrayList<Integer> userIds = new StringifyArrayList<>(recipients);
        qbEvent.setUserIds(userIds);
        qbEvent.setUserId(recipients.get(0));
        qbEvent.setType(QBEventType.ONE_SHOT);
        qbEvent.setId(userQbId);

        QBPushNotifications.createEvent(qbEvent).performAsync(new QBEntityCallback<QBEvent>() {
            @Override
            public void onSuccess(QBEvent qbEvent, Bundle args) {
                // sent
//                Toaster.shortToast("call notification success");
            }

            @Override
            public void onError(QBResponseException errors) {
//                Toaster.shortToast("call notification fail");
            }
        });



        qbEvent.setPushType(QBPushType.APNS_VOIP);
        qbEvent.setEnvironment(QBEnvironment.PRODUCTION);

        QBPushNotifications.createEvent(qbEvent).performAsync(new QBEntityCallback<QBEvent>() {
            @Override
            public void onSuccess(QBEvent qbEvent, Bundle args) {
                // sent
//                Toaster.shortToast("call notification success");
            }

            @Override
            public void onError(QBResponseException errors) {
//                Toaster.shortToast("call notification fail");
            }
        });


    }
}
