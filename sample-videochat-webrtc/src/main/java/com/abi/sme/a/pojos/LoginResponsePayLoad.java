package com.abi.sme.a.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yqlabs on 1/3/18.
 */

public class LoginResponsePayLoad {
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("Data")
    @Expose
    private Data data;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {

        @SerializedName("regid")
        @Expose
        private String regid;
        @SerializedName("abid")
        @Expose
        private String abid;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private Object password;
        @SerializedName("useridqb")
        @Expose
        private String useridqb;
        @SerializedName("deviceType")
        @Expose
        private String deviceType;
        @SerializedName("deviceToken")
        @Expose
        private String deviceToken;
        @SerializedName("deviceTokenQb")
        @Expose
        private Object deviceTokenQb;
        @SerializedName("unreadCount")
        @Expose
        private String unreadCount;
        @SerializedName("zone")
        @Expose
        private String zone;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("langId")
        @Expose
        private String langId;
        @SerializedName("langName")
        @Expose
        private String langName;
        @SerializedName("nativeZone")
        @Expose
        private String nativeZone;
        @SerializedName("userPreferableLanguages")
        @Expose
        private List<UserPreferableLanguage> userPreferableLanguages = null;

        public String getRegid() {
            return regid;
        }

        public void setRegid(String regid) {
            this.regid = regid;
        }

        public String getAbid() {
            return abid;
        }

        public void setAbid(String abid) {
            this.abid = abid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getPassword() {
            return password;
        }

        public void setPassword(Object password) {
            this.password = password;
        }

        public String getUseridqb() {
            return useridqb;
        }

        public void setUseridqb(String useridqb) {
            this.useridqb = useridqb;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public Object getDeviceTokenQb() {
            return deviceTokenQb;
        }

        public void setDeviceTokenQb(Object deviceTokenQb) {
            this.deviceTokenQb = deviceTokenQb;
        }

        public String getUnreadCount() {
            return unreadCount;
        }

        public void setUnreadCount(String unreadCount) {
            this.unreadCount = unreadCount;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getLangId() {
            return langId;
        }

        public void setLangId(String langId) {
            this.langId = langId;
        }

        public String getLangName() {
            return langName;
        }

        public void setLangName(String langName) {
            this.langName = langName;
        }

        public String getNativeZone() {
            return nativeZone;
        }

        public void setNativeZone(String nativeZone) {
            this.nativeZone = nativeZone;
        }

        public List<UserPreferableLanguage> getUserPreferableLanguages() {
            return userPreferableLanguages;
        }

        public void setUserPreferableLanguages(List<UserPreferableLanguage> userPreferableLanguages) {
            this.userPreferableLanguages = userPreferableLanguages;
        }

    }


    public static class UserPreferableLanguage {

        @SerializedName("langId")
        @Expose
        private Integer langId;
        @SerializedName("langCode")
        @Expose
        private String langCode;
        @SerializedName("langName")
        @Expose
        private String langName;

        public Integer getLangId() {
            return langId;
        }

        public void setLangId(Integer langId) {
            this.langId = langId;
        }

        public String getLangCode() {
            return langCode;
        }

        public void setLangCode(String langCode) {
            this.langCode = langCode;
        }

        public String getLangName() {
            return langName;
        }

        public void setLangName(String langName) {
            this.langName = langName;
        }

    }


}

