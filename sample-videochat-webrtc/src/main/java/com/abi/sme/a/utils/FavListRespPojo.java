package com.abi.sme.a.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yqlabs on 1/3/18.
 */

public class FavListRespPojo {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("Data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("userIdQB")
        @Expose
        private String userIdQB;
        @SerializedName("registerDate")
        @Expose
        private String registerDate;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("ratingCount")
        @Expose
        private Integer ratingCount;
        @SerializedName("callAvailability")
        @Expose
        private String callAvailability;
        @SerializedName("chatAvailability")
        @Expose
        private String chatAvailability;
        @SerializedName("zone")
        @Expose
        private String zone;
        @SerializedName("regId")
        @Expose
        private Integer regId;
        @SerializedName("deviceType")
        @Expose
        private Integer deviceType;
        @SerializedName("deviceToken")
        @Expose
        private String deviceToken;
        @SerializedName("deviceTokenQb")
        @Expose
        private String deviceTokenQb;
        @SerializedName("last_convId")
        @Expose
        private Object lastConvId;
        @SerializedName("fRegister")
        @Expose
        private String fRegister;
        @SerializedName("abuid")
        @Expose
        private String abuid;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("countryCode")
        @Expose
        private Object countryCode;
        @SerializedName("phoneNumber")
        @Expose
        private Object phoneNumber;
        @SerializedName("langId")
        @Expose
        private Integer langId;
        @SerializedName("langCode")
        @Expose
        private String langCode;
        @SerializedName("langName")
        @Expose
        private String langName;
        @SerializedName("topicPojos")
        @Expose
        private List<Object> topicPojos = null;
        @SerializedName("preferableLanguages")
        @Expose
        private List<Object> preferableLanguages = null;

        public String getUserIdQB() {
            return userIdQB;
        }

        public void setUserIdQB(String userIdQB) {
            this.userIdQB = userIdQB;
        }

        public String getRegisterDate() {
            return registerDate;
        }

        public void setRegisterDate(String registerDate) {
            this.registerDate = registerDate;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public Integer getRatingCount() {
            return ratingCount;
        }

        public void setRatingCount(Integer ratingCount) {
            this.ratingCount = ratingCount;
        }

        public String getCallAvailability() {
            return callAvailability;
        }

        public void setCallAvailability(String callAvailability) {
            this.callAvailability = callAvailability;
        }

        public String getChatAvailability() {
            return chatAvailability;
        }

        public void setChatAvailability(String chatAvailability) {
            this.chatAvailability = chatAvailability;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public Integer getRegId() {
            return regId;
        }

        public void setRegId(Integer regId) {
            this.regId = regId;
        }

        public Integer getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(Integer deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceTokenQb() {
            return deviceTokenQb;
        }

        public void setDeviceTokenQb(String deviceTokenQb) {
            this.deviceTokenQb = deviceTokenQb;
        }

        public Object getLastConvId() {
            return lastConvId;
        }

        public void setLastConvId(Object lastConvId) {
            this.lastConvId = lastConvId;
        }

        public String getFRegister() {
            return fRegister;
        }

        public void setFRegister(String fRegister) {
            this.fRegister = fRegister;
        }

        public String getAbuid() {
            return abuid;
        }

        public void setAbuid(String abuid) {
            this.abuid = abuid;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(Object countryCode) {
            this.countryCode = countryCode;
        }

        public Object getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(Object phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Integer getLangId() {
            return langId;
        }

        public void setLangId(Integer langId) {
            this.langId = langId;
        }

        public String getLangCode() {
            return langCode;
        }

        public void setLangCode(String langCode) {
            this.langCode = langCode;
        }

        public String getLangName() {
            return langName;
        }

        public void setLangName(String langName) {
            this.langName = langName;
        }

        public List<Object> getTopicPojos() {
            return topicPojos;
        }

        public void setTopicPojos(List<Object> topicPojos) {
            this.topicPojos = topicPojos;
        }

        public List<Object> getPreferableLanguages() {
            return preferableLanguages;
        }

        public void setPreferableLanguages(List<Object> preferableLanguages) {
            this.preferableLanguages = preferableLanguages;
        }

    }

}
