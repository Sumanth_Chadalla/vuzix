package com.abi.sme.a.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.abi.sme.a.R;


/**
 * Created by YQLabs on 27-06-2015.
 */
public class MyCustomProgressDialog extends ProgressDialog {
    private AnimationDrawable animation;
    private static String nmc;
    private static MyCustomProgressDialog dialog;
    Context context;

    public static MyCustomProgressDialog ctor(Context context) {
        dialog = new MyCustomProgressDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }

    public MyCustomProgressDialog(Context context) {
        super(context);
        this.context = context;

    }

    public MyCustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_custom_progress_dialog);
        ImageView la = (ImageView) findViewById(R.id.animation);
        la.setBackgroundResource(R.drawable.custom_progress_animation);
        animation = (AnimationDrawable) la.getBackground();
    }

    @Override
    public void show() {
        try {
            super.show();
            if (context != null)
                animation.start();
        }catch (Exception e){

        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if(animation!=null)
        animation.stop();
    }
}
