package com.abi.sme.a.utils;


import com.abi.sme.a.pojos.LoginRequestPayLoad;
import com.abi.sme.a.pojos.LoginResponsePayLoad;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by yqlabs on 1/3/18.
 */

public interface RestAPI {

    @POST(ApplicationContext.RELATIVE_PATH + ApplicationContext.LOGIN)
    Call<LoginResponsePayLoad> getLoginService(@Body LoginRequestPayLoad loginRequestPayLoad);

    @GET(ApplicationContext.RELATIVE_PATH + ApplicationContext.FAV_LIST_SERVICE)
    Call<FavListRespPojo> getFavoritesService(@Query("regId") String regId);
}
