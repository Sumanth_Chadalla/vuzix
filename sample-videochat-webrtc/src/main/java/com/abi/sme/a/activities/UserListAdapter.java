package com.abi.sme.a.activities;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.abi.sme.a.R;
import com.abi.sme.a.utils.ApplicationContext;
import com.abi.sme.a.utils.FavListRespPojo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.quickblox.sample.core.utils.UiUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yqlabs on 1/3/18.
 */

class UserListAdapter extends BaseAdapter {

    /*********** Declare Used Variables *********/
    private Context context;
    private List<FavListRespPojo.Datum> data;
    private static LayoutInflater inflater = null;
    public Resources res;

    /*************  CustomAdapter Constructor
     * @param
     * @param data*****************/
    public UserListAdapter(Context context, List<FavListRespPojo.Datum> data) {

        /********** Take passed values **********/
        this.context = context;
        this.data = data;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }


    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView opponentsName;
        public CircleImageView image_opponent_icon;

    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.item, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.opponentsName = (TextView) vi.findViewById(R.id.opponentsName);
            holder.image_opponent_icon=(CircleImageView) vi.findViewById(R.id.image_opponent_icon);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();



            /***** Get each Model object from Arraylist ********/

            /************  Set Model values in Holder elements ***********/

            holder.opponentsName.setText( data.get(position).getName());
        String imagUrl = data.get(position).getProfileImage() != null ? data.get(position).getProfileImage() : "http://";
//        try {
            Glide.with(context)
                    .load(ApplicationContext.BASE_URL + "/images/" + imagUrl)
                    .placeholder(R.drawable.dp_default)
                    .error(R.drawable.dp_default)
                    .override(200, 200)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image_opponent_icon);
//        }catch (Exception e){
//
//        }

            /******** Set Item Click Listner for LayoutInflater for each row *******/
            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ApplicationContext.callerName = data.get(position).getName();
                    ((OpponentsActivity) context).callToSelectedUser(position);
                }
            });
        return vi;
    }




}
