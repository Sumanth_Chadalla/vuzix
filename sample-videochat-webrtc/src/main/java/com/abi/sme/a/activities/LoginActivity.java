package com.abi.sme.a.activities;

import android.Manifest;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.abi.sme.a.R;
import com.abi.sme.a.pojos.LoginRequestPayLoad;
import com.abi.sme.a.pojos.LoginResponsePayLoad;
import com.abi.sme.a.utils.APIClient;
import com.abi.sme.a.utils.RestAPI;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.helper.Utils;
import com.quickblox.sample.core.utils.SharedPrefsHelper;
import com.quickblox.sample.core.utils.Toaster;
import com.abi.sme.a.services.CallService;
import com.abi.sme.a.utils.Consts;
import com.abi.sme.a.utils.QBEntityCallbackImpl;
import com.abi.sme.a.utils.UsersUtils;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private String TAG = LoginActivity.class.getSimpleName();

//    private EditText userNameEditText;
//    private EditText chatRoomNameEditText;

    private QBUser userForSave;
    private RestAPI apiInterface;
    private final int REQUEST_CODE_PERMISSION = 222;
    private final int REQUEST_READ_CODE_PERMISSION = 121;
    private boolean STORAGE_VAL = false;
    private boolean READ_STORAGE_VAL = false;
    private boolean CALL_PHONE_VAL = false;
    private boolean CAMERA_VAL = false;

    private ProgressDialog dialog;
    private String email,password;
    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        apiInterface = APIClient.getClient().create(RestAPI.class);

        initUI();
    }


    @Override
    protected View getSnackbarAnchorView() {
        return findViewById(R.id.root_view_login_activity);
    }

    private void initUI() {
        setActionBarTitle(R.string.title_login_activity);
//        userNameEditText = (EditText) findViewById(R.id.user_name);
//        userNameEditText.addTextChangedListener(new LoginEditTextWatcher(userNameEditText));
//
//        chatRoomNameEditText = (EditText) findViewById(R.id.chat_room_name);
//        chatRoomNameEditText.addTextChangedListener(new LoginEditTextWatcher(chatRoomNameEditText));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
//            case R.id.menu_login_user_done:
//                if (isEnteredUserNameValid()/* && isEnteredRoomNameValid()*/) {
//                    hideKeyboard();
////                    startSignUpNewUser(createUserWithEnteredData());
//                }
//                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    private boolean isEnteredRoomNameValid() {
//        return ValidationUtils.isRoomNameValid(this, chatRoomNameEditText);
//    }

//    private boolean isEnteredUserNameValid() {
//        return ValidationUtils.isUserNameValid(this, userNameEditText);
//    }

//    private void hideKeyboard() {
//        KeyboardUtils.hideKeyboard(userNameEditText);
//        KeyboardUtils.hideKeyboard(chatRoomNameEditText);
//    }

    private void startSignIn(final QBUser newUser) {
        showProgressDialog(R.string.dlg_creating_new_user);
        requestExecutor.signInUser(newUser, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser result, Bundle params) {
                        loginToChat(result);
                        loginToServer(result);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (e.getHttpStatusCode() == Consts.ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                            signInCreatedUser(newUser, true);
                        } else {
                            hideProgressDialog();
                            Toaster.longToast(R.string.sign_up_error);
                        }
                    }
                }
        );
    }

    private void loginToServer(final QBUser result) {
        LoginRequestPayLoad loginRequestPayLoad = new LoginRequestPayLoad();
        loginRequestPayLoad.setEmail(email);
        loginRequestPayLoad.setPassword(password);
        loginRequestPayLoad.setDeviceType("3");
        loginRequestPayLoad.setDeviceToken("");
        Call<LoginResponsePayLoad> call1 = apiInterface.getLoginService(loginRequestPayLoad);
        call1.enqueue(new Callback<LoginResponsePayLoad>() {
            @Override
            public void onResponse(Call<LoginResponsePayLoad> call, Response<LoginResponsePayLoad> response) {
                LoginResponsePayLoad responsePayLoad = response.body();
                if (responsePayLoad.getStatusCode().equalsIgnoreCase("200")) {
                    SharedPreferences sharedPreferences = getSharedPreferences("LoginData", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("regid", responsePayLoad.getData().getRegid());
                    editor.putString("abid", responsePayLoad.getData().getAbid());
                    editor.putString("name", responsePayLoad.getData().getName());
                    editor.putString("email", responsePayLoad.getData().getEmail());
                    editor.putString("useridqb", responsePayLoad.getData().getUseridqb());
                    editor.putString("profileImg", responsePayLoad.getData().getProfileImage());
                    editor.commit();

                    saveUserData(result);

                    startOpponentsActivity();
                }else {
                    Toast.makeText(getApplicationContext(), "Invalid authentication", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponsePayLoad> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void loginToChat(final QBUser qbUser) {
        qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);

        userForSave = qbUser;
        startLoginService(qbUser);
    }

    private void startOpponentsActivity() {
        OpponentsActivity.start(LoginActivity.this, false);
        finish();
    }

    private void saveUserData(QBUser qbUser) {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
        sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, "");
        sharedPrefsHelper.saveQbUser(qbUser);
    }

//    private QBUser createUserWithEnteredData() {
//        return createQBUserWithCurrentData(String.valueOf(userNameEditText.getText()));
//    }

    private QBUser createQBUserWithCurrentData(String email) {
        QBUser qbUser = null;
        if (!TextUtils.isEmpty(email)) {
            StringifyArrayList<String> userTags = new StringifyArrayList<>();

            qbUser = new QBUser();
//            qbUser.setFullName(email);
            qbUser.setLogin(email);
            qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
//            qbUser.setTags(userTags);
        }

        return qbUser;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Consts.EXTRA_LOGIN_RESULT_CODE) {
            hideProgressDialog();
            boolean isLoginSuccess = data.getBooleanExtra(Consts.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(Consts.EXTRA_LOGIN_ERROR_MESSAGE);

            if (isLoginSuccess) {
//                saveUserData(userForSave);

                signInCreatedUser(userForSave, false);
            } else {
                Toaster.longToast(getString(R.string.login_chat_login_error) + errorMessage);
//                userNameEditText.setText(userForSave.getFullName());
//                chatRoomNameEditText.setText(userForSave.getTags().get(0));
            }
        }
        if (resultCode == RESULT_OK /*&& requestCode == 101*/) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            String base64 = result.getContents();
            byte[] scanedData = Base64.decode(base64, Base64.DEFAULT);
            try {
                String jsonString = new String(scanedData, "UTF-8");
                JSONObject jsonObject = new JSONObject(jsonString);
                email = jsonObject.getString("email");
                password = jsonObject.getString("pwd");
                startSignIn(createQBUserWithCurrentData(email));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void signInCreatedUser(final QBUser user, final boolean deleteCurrentUser) {
        requestExecutor.signInUser(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser result, Bundle params) {
//                if (deleteCurrentUser) {
//                    removeAllUserData(result);
//                } else {
                    startOpponentsActivity();
//                }
            }

            @Override
            public void onError(QBResponseException responseException) {
                hideProgressDialog();
                Toaster.longToast(R.string.sign_up_error);
            }
        });
    }

    private void removeAllUserData(final QBUser user) {
        requestExecutor.deleteCurrentUser(user.getId(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                UsersUtils.removeUserData(getApplicationContext());
//                startSignUpNewUser(createUserWithEnteredData());
            }

            @Override
            public void onError(QBResponseException e) {
                hideProgressDialog();
                Toaster.longToast(R.string.sign_up_error);
            }
        });
    }

    private void startLoginService(QBUser qbUser) {
        Intent tempIntent = new Intent(this, CallService.class);
        PendingIntent pendingIntent = createPendingResult(Consts.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
        CallService.start(this, qbUser, pendingIntent);
    }

    private String getCurrentDeviceId() {
        return Utils.generateDeviceId(this);
    }

    public void scanQRCode(View view) {
        if (checkInternet()) {
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                checkAllPermissionsVal();
//            } else {
//                CAMERA_VAL = true ; STORAGE_VAL = true;  READ_STORAGE_VAL = true; CALL_PHONE_VAL =true;
                moveToQRCodeIntent();

//            }

        } else {
            Toast.makeText(getApplicationContext(),"please check internet connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void moveToQRCodeIntent() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan a barcode");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(true);
        integrator.initiateScan();
    }

    private class LoginEditTextWatcher implements TextWatcher {
        private EditText editText;

        private LoginEditTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            editText.setError(null);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    protected boolean checkInternet() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    private void checkAllPermissionsVal() {
        int readpermission = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writepermission = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int camerapermission = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CAMERA);
        int callphonepermission = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CALL_PHONE);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != -1) {
            CAMERA_VAL = true;
        }
        if (readpermission != -1) {
            READ_STORAGE_VAL = true;
        }
        if (writepermission != -1) {
            STORAGE_VAL = true;
        }
        if(callphonepermission !=-1){
            CALL_PHONE_VAL = true;
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (callphonepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_CODE_PERMISSION);
            }
            return;
        }
        if (listPermissionsNeeded.isEmpty()) {
            CAMERA_VAL = true;
            STORAGE_VAL = true;
            READ_STORAGE_VAL = true;
            CALL_PHONE_VAL = true;
            CAMERA_VAL = true ; STORAGE_VAL = true;  READ_STORAGE_VAL = true; CALL_PHONE_VAL =true;
            moveToQRCodeIntent();
        }
        return;
    }

    private void checkAllReadPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Explain to the user why we need to read the contacts
                }

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_READ_CODE_PERMISSION);

                // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                // app-defined int constant that should be quite unique

                return;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {

            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {

                    if (permissions[i].equals(Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "camera granted");
                            CAMERA_VAL = true;
                            moveToQRCodeIntent();

                        }
                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "write storage granted");
                            STORAGE_VAL = true;
                            moveToQRCodeIntent();
                        }
                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "read storage granted");
                            READ_STORAGE_VAL = true;
                            moveToQRCodeIntent();
                        }
                    }else if (permissions[i].equals(Manifest.permission.CALL_PHONE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "read storage granted");
                            CALL_PHONE_VAL = true;
                            moveToQRCodeIntent();
                        }
                    }
                }
            } else {

            }
        } else if (requestCode == REQUEST_READ_CODE_PERMISSION) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "read storage granted");
                        }
                    }
                }
            } else {

            }
        }
    }


}
