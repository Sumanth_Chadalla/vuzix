package com.abi.sme.a.utils;

/**
 * Created by yqlabs on 1/3/18.
 */

public class ApplicationContext {

//    public static String BASE_URL = "http://192.168.1.2:9090";
    public static String BASE_URL = "http://app02.yqlabs.com";
//    public static final String RELATIVE_PATH = "/SME_SERVICE_EP/rest/";
//    public static final String RELATIVE_PATH = "/ABSME_EP_TEST/rest/";
    public static final String RELATIVE_PATH = "/SME_SERVICE_EP/rest/";


    public static final String LOGIN = "loginService";
    public static final String FAV_LIST_SERVICE = "favouritesListService";

    public static String callerName;
}
